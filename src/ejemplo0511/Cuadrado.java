/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0511;

/**
 * Fichero: Cuadrado.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
public class Cuadrado {

  private int lado;

  Cuadrado(int l) {
    lado = l;
  }

  public int getArea() {
    return lado * lado;
  }
}