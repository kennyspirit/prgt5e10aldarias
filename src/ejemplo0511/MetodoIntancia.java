/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0511;

/**
 * Fichero: MetodoIntancia.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
public class MetodoIntancia {

  public static int var;
  public int var2;

  public void prueba() { // Metodo Instancia
    var = 3;  // miembro de la clase
    var2 = 5;  // miembro instancia
  }

  public static void main(String args[]) {
    MetodoIntancia t = new MetodoIntancia();
    t.prueba();
  }
}
