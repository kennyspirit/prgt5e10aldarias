/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0509;

import ejemplo0509.Rectangulo;

/**
 * Fichero: TestClone.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */

public class TestClone {


   public static void main( String [] args ) {
      Rectangulo r1=new Rectangulo(5,7);
      Rectangulo r2= (Rectangulo) r1.clone();
      Rectangulo r3=r1;  // Copia direccion memoria

      r1.muestra();
      r2.muestra();
      r3.muestra();

      if ( r1.equals(r2) ) System.out.println("iguales");
      else  System.out.println("no iguales");      

      if ( r1.equals(r3) )  System.out.println("iguales");
      else System.out.println("no iguales");
      
      r1.incrementarAncho();
      System.out.print("Nombre: "+r1.toString()+" ");r1.muestra();
      System.out.print("Nombre: "+r2.toString()+" ");r2.muestra();
      System.out.print("Nombre: "+r3.toString()+" ");r3.muestra();

      if (  r1.equals(r1) ) System.out.println("iguales");
      else  System.out.println("no iguales");
   }
}
/* EJECUCION
Ancho: 5 Alto: 7
Ancho: 5 Alto: 7
Ancho: 5 Alto: 7
no iguales
iguales
Nombre: rectangulo@30c221 Ancho: 6 Alto: 7
Nombre: rectangulo@119298d Ancho: 5 Alto: 7
Nombre: rectangulo@30c221 Ancho: 6 Alto: 7
iguales
*/