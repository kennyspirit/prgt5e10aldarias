/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0524;

/**
 * Fichero: RectanguloSobrecargado.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class RectanguloSobrecargado {
  private int ancho = 0;
  private int alto = 0;

  RectanguloSobrecargado ( ) {
    ancho=0;
    this.alto=0;
    System.out.println("Constructor 1");
  }

  RectanguloSobrecargado ( int dato) {
	System.out.println("Constructor 2");
    ancho=alto=dato;
  }

  RectanguloSobrecargado ( int an, int al) {
    ancho=an;
    this.alto=al;
    System.out.println("Constructor 3");
  }


  public void muestra() {
    System.out.println("Ancho: "+ancho+ " Alto: "+this.alto);
  }

  public static void main ( String  args[] ) {
    // rectanguloSobrecargado x;
    RectanguloSobrecargado a = new RectanguloSobrecargado();
    RectanguloSobrecargado b = new RectanguloSobrecargado(8);
    RectanguloSobrecargado c = new RectanguloSobrecargado(2,3);
    // x.muestra();
    a.muestra();
    b.muestra();
    c.muestra();
  }
}
/* EJECUCION:
Constructor 1
Constructor 2
Constructor 3
Ancho: 0 Alto: 0
Ancho: 8 Alto: 8
Ancho: 2 Alto: 3
*/
