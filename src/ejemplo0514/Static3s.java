/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0514;

/**
 * Fichero: Static3s.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Static3s {

  public static int dato; 

  public static void metodo() { 
    dato++; 
  }

  public static void main ( String [] args ) {
	metodo(); 
  }
}
