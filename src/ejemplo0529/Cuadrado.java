package ejemplo0529;

/**
 * Fichero: Rectangulo.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */

public class Cuadrado implements Figura {
   private int lado;
   
   Cuadrado (int l) {
      this.lado=l;
   }

   public int area() {
      return lado*lado;
   }
  
  public int perimetro() {
      return 4*lado;
   }
   
}

