/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0513;

/**
 * Fichero: Static2s.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Static2s {

  public int dato = 1; // variable no static

  public void metodo() {  // método no static
    dato++;
  }

  public static void main(String[] args) {
    Static2s a = new Static2s(); // Creamos objeto
    Static2s b = new Static2s();
    a.metodo();
    b.metodo();
    System.out.println(a.dato);
    System.out.println(b.dato);
  }
}