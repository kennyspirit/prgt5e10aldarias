/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0512;

/**
 * Fichero: Static1.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Static1 {

  public static int dato;  // Variable static

  public static void metodo() {
    this.dato++; // Error  al usar this
  }

  public static void main ( String [] args ) {
    metodo();
  }
}
