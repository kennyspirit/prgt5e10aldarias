/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0526;

/**
 * Fichero: ConstructorCopia.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class ConstructorCopia {

  private int ancho = 0;
  private int alto = 0;

  ConstructorCopia ( ) {
    ancho=alto=0;
  }
  ConstructorCopia ( int dato) {
    ancho=alto=dato;
  }
  ConstructorCopia ( int an, int al) {
    ancho=an;
    this.alto=al;
  }
  ConstructorCopia ( ConstructorCopia r) {
    ancho=r.ancho;
    alto=r.alto;
  }

  public void incrementaAncho ( ) {
    ancho++;
  }
  public void incrementaAlto ( ) {
    alto++;
  }
  public void muestra() {
    System.out.println("Ancho: "+ancho+ " Alto: "+this.alto);
  }

  public static void main ( String [] args) {
    ConstructorCopia r1 = new ConstructorCopia(5,7);
    ConstructorCopia r2 = new ConstructorCopia(r1);
    r2.incrementaAncho();
    r2.incrementaAlto();
    r1.muestra();
    r2.muestra();
  }

}